var express = require('express');
var mongoose = require('mongoose');
var logger = require('morgan');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 8080;
var foodApi = require('./server/routes/food.js');
var db = mongoose.connection;

// require env variables
require('dotenv').config();

mongoose.Promise = Promise;

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
  res.setHeader('Access-Control-Allow-Headers',
    'Access-Control-Allow-Headers,' +
    ' x-app-id,' +
    ' x-app-key,' +
    ' Origin,Accept,' +
    ' X-Requested-With,' +
    ' Content-Type,' +
    ' Access-Control-Request-Method,' +
    ' Access-Control-Request-Headers');
  res.setHeader('Cache-Control', 'no-cache');
  next();
});

// Configure our app for morgan and body parser
app.use(logger('dev'));
app.use(bodyParser.urlencoded({
  extended: true
}));

// include api routes
app.use('/api/v1/food', foodApi);


app.use(bodyParser.json());
app.use(express.static('public'));
app.use('/styles', express.static(`${__dirname}/node_modules/base-components/src/static/`));
app.use('/react-s-alert', express.static(`${__dirname}/node_modules/react-s-alert/dist/`));

// connection to mlab
mongoose.connect(`mongodb://${process.env.USERNAME}:${process.env.PASSWORD}@${process.env.INVENTORY_DB_URL}`);

// Log any mongoose errors
db.on('error', error => {
  console.log('Mongoose Error: ', error);
});

// Log a success message when we connect to our mongoDB collection with no issues
db.once('open', () => {
  console.log('Mongoose connection successful.');
});

app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});
