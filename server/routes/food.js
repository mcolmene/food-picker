var express = require('express');
var router = express.Router();
var path = require('path');
var queryString = require('query-string');
var request = require('request');

// require the model
var Food = require("../models/food.js");

// returns all the food data
router.get('/', function (req, res) {
  Food.find({}, function (error, doc) {
    // Log any errors
    if (error) {
      console.log(error);
    }
    // Or send the doc to the browser as a json object
    else {
      res.send(doc);
    }
  });
});

// global save function to use for custom and formatted food api
function saveFood(food, res) {
  food.save(function (error, doc) {
    // Send any errors to the browser
    if (error) {
      res.status(400).send(error);
    }
    // Otherwise, send the new doc to the browser
    else {
      res.status(200).send(doc);
    }
  });
}

// saves the new food created by user to the db
router.post('/custom', function (req, res) {
  var food = new Food(req.body);
  saveFood(food, res);
});

// data for the auto complete input box when adding a food
router.post('/instant', function (req, res) {
  request({
    uri: `https://trackapi.nutritionix.com/v2/search/instant?query=${req.body.query}&branded=false`,
    headers: {
      'x-app-id': '6a1daf64',
      'x-app-key': 'c6e3de13df82c95cdb7a40550c62ebdf',
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    method: 'GET'
  }, (err, response, data) => {
    if (err) {
      res.send(err)
    } else {
      res.status(200).send(JSON.parse(data))
    }
  })
});

router.post('/formatted', function (req, res) {
  request({
    headers: {
      'x-app-id': '6a1daf64',
      'x-app-key': 'c6e3de13df82c95cdb7a40550c62ebdf',
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    uri: 'https://www.nutritionix.com/track-api/v2/natural/nutrients',
    body: queryString.stringify({query: req.body.query}),
    method: 'POST'
  }, (err, response, data) => {
    if (err) {
      res.send(err);
    } else {
      var result = JSON.parse(data);
      console.log(result);
      var food = result.foods[0];
      const foodObj = {
        name: food.food_name,
        amount: req.body.amount,
        amountType: req.body.amountType,
        image: food.photo.thumb,
        nutrition: {
          Calories: food.nf_calories,
          'Serving Size': `${food.serving_weight_grams}g`,
          'Carbohydrates': food.nf_total_carbohydrate,
        }
      }
      var food = new Food(foodObj);
      saveFood(food, res)
    }
  });
});
router.put('/', function (req, res) {
  const doc = {
    amount: req.body.amount,
    amountType: req.body.amountType,
  };
  Food.findOneAndUpdate({_id: req.body.id}, {$set: doc}, {new: true}, (err, result) => {
    if (err) {
      res.send(err);
    }
    // returns updated document
    res.send(result);
  });
});
router.delete('/', function (req, res) {
  Food.findByIdAndRemove(req.body.id, function (err, food) {
    var response = {
      message: "Food successfully deleted",
      id: food._id
    };
    res.send(response);
  });
});

module.exports = router;
