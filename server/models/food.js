var mongoose = require("mongoose");

// Create the Schema class
var Schema = mongoose.Schema;

var FoodSchema = new Schema({
  name: {
    type: String,
    trim: true,
    required: [true, "Name is Required"]
  },
  amount: {
    type: String,
    trim: true,
    required: [true, "Amount is Required"]
  },
  amountType: {
    type: String,
    trim: true,
    required: [true, "Amount Type is Required"]
  },
  image: {
    type: String,
    trim: true,
  },
  nutrition: {
    Calories: {
      type: String,
      trim: true,
    },
    'Serving Size': {
      type: String,
      trim: true,
    },
    'Carbohydrates': {
      type: String,
      trim: true,
    }
  }
});

var Food = mongoose.model("Food", FoodSchema, "ingredients");

// Export the User model, so it can be used in server.js with a require
module.exports = Food;