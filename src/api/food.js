
const API_INVENTORY_URL = '/api/v1/food';
const API_SEARCH_INSTANT = '/api/v1/food/instant';
const API_ADD_FORMATTED_ITEM = '/api/v1/food/formatted';
const API_ADD_CUSTOM_ITEM = '/api/v1/food/custom';

// lint ignore was added because $ was not defined in this scope but it is defined in index.html
/* eslint-disable no-undef */
const foodAPI = {
  getInventory: () => new Promise((resolve, reject) => {
    $.get(API_INVENTORY_URL)
      .then(response => resolve(response))
      .catch(error => reject(error));
  }),
  deleteItem: event => new Promise((resolve, reject) => {
    const itemId = event.target.getAttribute('value');
    const itemName = event.target.getAttribute('name');
    $.ajax({
      url: API_INVENTORY_URL,
      type: 'DELETE',
      data: { id: itemId },
      success: () => resolve(itemName), // return the name of the item deleted
      error: error => reject(error)
    });
  }),
  updateItem: (id, amount, amountType) => new Promise((resolve, reject) => {
    $.ajax({
      url: API_INVENTORY_URL,
      type: 'PUT',
      data: {
        id,
        amount,
        amountType
      },
      success: res => resolve(res),
      error: error => reject(error)
    });
  }),
  addFormattedItem: request => new Promise((resolve, reject) => {
    $.post(API_ADD_FORMATTED_ITEM, request)
      .then(res => resolve(res))
      .catch(error => reject(error));
  }),
  addCustomItem: request => new Promise((resolve, reject) => {
    $.post(API_ADD_CUSTOM_ITEM, request)
      .then(res => resolve(res))
      .catch(error => reject(error));
  }),
  searchItems: request => new Promise((resolve, reject) => {
    $.post(API_SEARCH_INSTANT, request)
      .then(results => resolve(results))
      .catch(error => reject(error));
  })
};
/* eslint-enable no-undef */
export default foodAPI;
