import React from 'react';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';

import Container from './common/Container';
import Create from './components/inventory';
import Recipe from './components/recipes';
import About from './components/about/About';
import NotFound from './components/404';

const Routes = () => (
  <Router history={hashHistory}>
    <Route path="/" component={Container} >
      <IndexRoute component={About} />
      <Route path="inventory" component={Create} />
      <Route path="recipe" component={Recipe} />
    </Route>
    <Route path="*" component={NotFound} />
  </Router>
  );
export default Routes;
