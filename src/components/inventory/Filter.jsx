import React from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import Header from '../../common/Header';

import Input from 'base-components/build/components/Input';
import Button from 'base-components/build/components/Button';

const Filter = ({data, onChange, onCheckboxClick, onClose, search, outOfStock}) =>
  <div className={`container createForm`}>
    <Header title="Filter" />
    <div className={`formContainer`}>
      <div className="col-sm-6">
        <Input
          value={search}
          onChange={onChange}
          name="search"
          id="search"
          type="text"
          placeholder="Search"
          className="form-control"
          title="(Optional) Search"
        />
        <span>Results: {data.length}</span>
      </div>
      <div className="col-sm-6">
        <div className="form-check">
          <label className="form-check-label">
            <Input
              onChange={onCheckboxClick}
              id="outOfStock"
              type="checkbox"
              className="form-check-input"
              title="Out of Stock"
              checked={outOfStock}
            />
              Out of Stock
          </label>
        </div>
      </div>
    </div>
    <div className="margin-2" style={{overflow: 'auto'}}>
      <Button
        title="Close"
        className="btn btn-outline-dark float-r"
        onClick={onClose}
        label="Close"
      />
    </div>
  </div>;

export default Filter;

Filter.propTypes = {
  dataLength: 3,
  onChange: noop,
  onCheckboxClick: noop,
  onClose: noop,
  search: '',
  outOfStock: false,
};
Filter.defaultProps = {
  dataLength: PropTypes.number,
  onChange: PropTypes.func,
  onCheckboxClick: PropTypes.func,
  onClose: PropTypes.func,
  search: PropTypes.string,
  outOfStock: PropTypes.bool,
};
