import React from 'react';
import PropTypes from 'prop-types';

const ItemList = ({ name, image, amount, index, amountType }) => (
  <tr style={(amount === '0') ? {backgroundColor: '#eeeeee'} : {}}>
    <td>
      <div className="row">
        <div className="col">
          {(image)
            ? <img
              src={image}
              alt={name}
              style={{width: 75, height: 75}}/>
            : <img
              src="http://cumbrianrun.co.uk/wp-content/uploads/2014/02/default-placeholder.png"
              alt="No photo"
              style={{width: 75, height: 75}}
            />
          }
        </div>
        <div className="col-11">
          <h5 className="mt-1">{name}</h5>
        </div>
      </div>
    </td>
    <td
      className="align-middle"
      style={(amount === '0') ? { color: 'red' } : {}}
    >
      {(amount === '0') ? 'Out of Stock' : `${amount} ${amountType}`}
    </td>
  </tr>
);

export default ItemList;

ItemList.propTypes = {
  name: PropTypes.string,
  amount: PropTypes.string,
  image: PropTypes.string,
  amountType: PropTypes.string,
  index: PropTypes.string,
};

ItemList.defaultProps = {
  name: '',
  nutrition: {},
  amount: '',
  image: '',
  amountType: '',
};
