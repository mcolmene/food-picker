import React, {Component} from 'react';
import {noop} from 'lodash';
import PropTypes from 'prop-types';
import Button from "base-components/build/components/Button";
import ItemDetails from './ItemDetails';
import ItemList from './ItemList';


export default class Inventory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: '',
      isFlipped: false,
      editing: false,
      view: 'grid',
    };
    this.viewChangeClick = this.viewChangeClick.bind(this);
    this.onDetailsClick = this.onDetailsClick.bind(this);
    this.onEditClick = this.onEditClick.bind(this);
    this.onEditCancelClick = this.onEditCancelClick.bind(this);
    this.onHideDetails = this.onHideDetails.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
  }

  viewChangeClick(e) {
    const name = e.target.getAttribute('name');
    if (this.state.view !== name) {
      this.setState({view: name});
    }
  }

  onHideDetails() {
    this.setState({selectedRow: '', isFlipped: false})
  }

  onDetailsClick(e) {
    const selectedRow = e.target.getAttribute('value');
    this.setState({selected: selectedRow, isFlipped: true, editing: false})
  }

  onEditClick() {
    this.setState({editing: !this.state.editing})
  }

  onEditCancelClick() {
    this.setState({editing: false})
  }

  onDelete(e) {
    this.setState({selected: '', isFlipped: false, editing: false});
    this.props.onDelete(e);
  }

  onUpdate(formData) {
    this.setState({selected: '', isFlipped: false, editing: false});
    this.props.onUpdate(formData)
  }

  render() {
    const {data, onAddClick, onFilterClick} = this.props;
    const {isFlipped, selected, editing} = this.state;
    let content;
    if (data === 'loading') {
      content = <div>loading</div>
    } else if (typeof data === 'object') {
      const foodData = data.map((ingredient, index) => (
        (this.state.view === 'grid')
          ? (
            <ItemDetails
              {...ingredient}
              key={`${ingredient.name}${index}`}
              selected={selected === `${index}`}
              index={index}
              isFlipped={selected === `${index}` && isFlipped}
              onClick={this.onClick}
              onUpdate={this.onUpdate}
              onDetailsClick={this.onDetailsClick}
              onEditCancelClick={this.onEditCancelClick}
              onHideDetails={this.onHideDetails}
              onDelete={this.onDelete}
              id={ingredient._id}
              onEditClick={this.onEditClick}
              editing={editing}
            />
          )
          : (
            <ItemList
              key={`${ingredient.name}${index}`}
              selected={selected === `${index}`}
              index={index}
              id={ingredient._id}
              {...ingredient}
            />
          )
      ));
      content = (
        (this.state.view === 'grid')
          ? (
            <div className="row">
              {foodData}
            </div>
          )
          : (
            <div className="container mt-3 p-0">
              <table className="table">
                <thead>
                <tr>
                  <th scope="col">Name</th>
                  <th scope="col">Qty</th>
                </tr>
                </thead>
                <tbody>
                {foodData}
                </tbody>
              </table>
            </div>
          )
      )
    }
    return (
      <div>
        <Button
          className="btn btn-outline-primary"
          title="Add"
          label={<span className="oi oi-plus" onClick={onAddClick}/>}
          onClick={onAddClick}
        />
        <Button
          className="btn btn-outline-dark margin-l-1"
          title="Filter"
          label={<span className="oi oi-magnifying-glass" onClick={onFilterClick}/>}
          onClick={onFilterClick}
        />
        <Button
          className={`btn ${(this.state.view === 'grid') ? 'btn-primary active' : 'btn-outline-dark'} margin-l-1`}
          label={<span className="oi oi-grid-three-up" onClick={this.viewChangeClick} name="grid"/>}
          title="Grid View"
          name="grid"
          onClick={this.viewChangeClick}
        />
        <Button
          className={`btn ${(this.state.view === 'list') ? 'btn-primary active' : 'btn-outline-dark'} margin-l-1`}
          label={<span className="oi oi-list" onClick={this.viewChangeClick} name="list"/>}
          title="List View"
          name="list"
          onClick={this.viewChangeClick}
        />
        {content}
      </div>
    )
  }
}

Inventory.propTypes = {
  onUpdate: PropTypes.func,
  data: PropTypes.array,
  onAddClick: PropTypes.func,
  onFilterClick: PropTypes.func,
  onDelete: PropTypes.func,
  search: PropTypes.string,
};

Inventory.defaultProps = {
  onUpdate: noop,
  data: [],
  onAddClick: noop,
  onFilterClick: noop,
  onDelete: noop,
  search: '',
};
