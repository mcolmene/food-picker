import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import Loader from 'react-loader';
// TODO: having issues with index file will have to check on that
import Input from 'base-components/build/components/Input';
import Dropdown from 'base-components/build/components/Dropdown';
import Button from 'base-components/build/components/Button';
import styles from '../../static/foodpicker.css'
import Typeahead from './AsyncTypeahead';

import foodAPI from '../../api/food';

export default class CreateItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      type: '',
      amount: '',
      amountType: '',
      selectedRow: '',
      selectedItem: '',
      loading: false,
      options: []
    };
    this.resetState = this.state;
    this.onChange = this.onChange.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.onClick = this.onClick.bind(this);
    this.userSelected = this.userSelected.bind(this);
  }

  onChange(e) {
    let newState = {
      [e.target.name]: e.target.value
    };
    this.setState(newState);
  }

  onSearch (query) {
    const request = {
     query
    };
    foodAPI.searchItems(request)
      .then(results => {
        const common = results.common;
        return (
          this.setState({
            options: common
          })
        )
      })
      .catch( error => {
        console.log(error);
      })
  }
  userSelected(selectedItem) {
    this.setState({selectedItem});
  }
  onClick(e) {
    e.preventDefault();
    this.setState({ loading: true }, () => {
      const request = {
          query: this.state.selectedItem,
          amount: this.state.amount,
          amountType: this.state.amountType
        };
      foodAPI.addFormattedItem(request)
        .then( res => {
          this.props.onClick(e, this.state, res);
        })
        .catch( error => {
          console.log(error)
        });
    });
  }
  render() {
    return(
      <form>
        <div className={`row container`}>
          <div className="col-12">
          <Typeahead
            placeholder="Enter food name"
            userSelected={this.userSelected}
            onSearch={this.onSearch}
            options={this.state.options}
          />
          </div>
        </div>
          <div className="row container margin-tb-2">
          <div className="col-6 col-md-3">
             <Input
              value={this.state.amount}
              name="amount"
              id="amount"
              type="text"
              title="Please enter an amount"
              maxLength="5"
              placeholder="Amount"
              className="form-control"
              required
              onChange={this.onChange}
            />
          </div>
          <div className="col-6 col-md-3">
            <Dropdown
              name="amountType"
              value={this.state.amountType}
              onChange={this.onChange}
              className="form-control"
              options={['Quantity', 'oz', 'count']}
            />
          </div>
        </div>
        <div className="container">
        <div className="row mt-4 mb-2">
          <div className="col-12 col-sm-6 col-md-3">
            <Loader
              loaded={!this.state.loading}
            >
              <Button
                type="submit"
                title="Submit"
                className={`btn btn-primary margin-b-2 ${styles.btnMax}`}
                onClick={this.onClick}
                label="Submit"
              />
            </Loader>
          </div>
          <div className="col-12 col-sm-6 col-md-3">
            <Button
              title="Close"
              className={`btn btn-outline-dark ${styles.btnMax}`}
              onClick={this.props.onClose}
              label="Close"
            />
          </div>
        </div>
        </div>
      </form>
    )
  }
}

CreateItem.propTypes = {
  onClick: PropTypes.func,
  onClose: PropTypes.func,
};

CreateItem.defaultProps = {
  onClick: noop,
  onClose: noop,
};
