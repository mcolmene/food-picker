import React, { Component } from 'react'
import Alert from 'react-s-alert';

import Header from '../../common/Header';
import Filter from './Filter';
import CreateForm from './CreateItem'
import Inventory from './Inventory'
import foodAPI from '../../api/food';

const validateForm = state => {
  // Capatalize the first letter
  state.name = state.name.charAt(0).toUpperCase() + state.name.slice(1);

  return state;
};

export default class View extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: 'loading',
      isAddOpen: false,
      isFilterOpen: false,
      search: '',
      outOfStock: false,
    };
    this.onClick = this.onClick.bind(this);
    this.onClose = this.onClose.bind(this);
    this.onCheckboxClick = this.onCheckboxClick.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onAddClick = this.onAddClick.bind(this);
    this.onFilterClick = this.onFilterClick.bind(this);
    this.onUpdate = this.onUpdate.bind(this);
  }
  onCheckboxClick() {
    this.setState({ outOfStock: !this.state.outOfStock })
  }
  onAddClick() {
    this.setState({ isAddOpen: true, isFilterOpen: false })
  }
  onFilterClick() {
    this.setState({ isAddOpen: false, isFilterOpen: true })
  }
  onClose() {
    this.setState({ isAddOpen: false, isFilterOpen: false })
  }
  onClick(e, state, res) {
    e.preventDefault();
    validateForm(state);
    Alert.success(`Successfully added '${res.name}' to the list!`, {
        position: 'top-right',
        effect: 'flip',
        timeout: 2000
    });
    foodAPI.getInventory()
      .then( res => {
        const newState = this.state;
        newState.data = res;
        newState.isAddOpen = false;
        this.setState(newState)
      })
      .catch( error => {
        console.log(error);
      })
  }
  onUpdate(formData) {
    const { id, amount, amountType } = formData;
    foodAPI.updateItem(id, amount, amountType)
      .then(result => {
        Alert.success(`Successfully updated '${result.name}'`, {
          position: 'top-right',
          effect: 'flip',
          timeout: 2000
        });
        foodAPI.getInventory()
          .then( res => {
            const newState = this.state;
            newState.data = res;
            this.setState(newState)
          })
          .catch( error => {
            console.log(error)
          })
      })
      .catch(error => {
        console.log(error)
      })
  }
  onDelete(e) {
    foodAPI.deleteItem(e)
      .then(itemName => {
        Alert.error(`Successfully removed '${itemName}' from the list!`, {
          position: 'top-right',
          effect: 'flip',
          timeout: 2000
        });
        foodAPI.getInventory()
          .then( res => {
            const newState = this.state;
            newState.data = res;
            this.setState(newState)
          })
          .catch( error => {
            console.log(error)
          })
      })
      .catch( error => {
        console.log(error)
      });
  }
  onChange(e) {
    this.setState({ search: e.target.value })
  }
  componentWillMount() {
    foodAPI.getInventory()
      .then((res) => {
        this.setState({ data: res })
      })
      .catch(error => {
        console.log(error)
      })
  }
  render() {
    const { search, data } = this.state;
    // filter the data based on search criteria if applicable
    let filteredData = data !== 'loading' && data.filter((ingredient) => {
      return (search === '' || ingredient.name.toLowerCase().indexOf(search.toLowerCase()) >= 0)
    });
    if(this.state.outOfStock) {
      filteredData = filteredData.filter((ingredient) => {
        console.log(ingredient.amount === '0');
        return ingredient.amount === '0'
      })
    }
    return(
      <div className="col-12">
        <h3 className="center">Food Inventory</h3>
        {
          this.state.isAddOpen && (
            <div className={`container-2 createForm`}>
              <Header title="Add a new food" />
              <CreateForm onClick={this.onClick} onClose={this.onClose} />
            </div>
          )
        }
        { this.state.isFilterOpen && (
          <Filter
            data={filteredData}
            onChange={this.onChange}
            onCheckboxClick={this.onCheckboxClick}
            onClose={this.onClose}
            search={this.state.search}
            outOfStock={this.state.outOfStock}
          />
        )}
        <Alert stack={{limit: 3}} />
        { !this.state.isAddOpen &&
          <div className={`inventoryContainer margin-b-4 pad-0`}>
            <Inventory
              data={filteredData}
              onDelete={this.onDelete}
              onAddClick={this.onAddClick}
              onFilterClick={this.onFilterClick}
              onUpdate={this.onUpdate}
              search={this.state.search}
            />
          </div>
        }
      </div>
    )
  }
}
