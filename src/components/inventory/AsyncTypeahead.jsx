import React from 'react';
import {asyncContainer, Typeahead} from 'react-bootstrap-typeahead';
const Search = asyncContainer(Typeahead);

const AsyncTypeahead = ({userSelected, onSearch, options, placeholder}) => {
    return(
        <Search
        placeholder={placeholder}
        labelKey="food_name"
        onInputChange={userSelected}
        onSearch={onSearch}
        options={options}
        maxHeight={200}
        align="right"
        className="typeahead-menu"
        renderMenuItemChildren={(result, props) => (
          <span>
            { (result.photo.thumb) 
              ? <img src={result.photo.thumb} alt={result.food_name} style={{ width: 40 }} />
              : <img src="http://cumbrianrun.co.uk/wp-content/uploads/2014/02/default-placeholder.png" alt="No photo" style={{ width: 40 }} />
             }
            <span className="ml-2">{result.food_name}</span>
          </span>
        )}
      />
    )
}

export default AsyncTypeahead;