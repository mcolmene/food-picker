import React from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import Button from 'base-components/build/components/Button';
import ReactCardFlip from '../../common/ReactCardFlip';
import EditForm from './EditItem';

const ItemDetails = ({
    name,
    nutrition,
    amount,
    image,
    amountType,
    selected,
    index,
    onClick,
    id,
    onDelete,
    isFlipped,
    onDetailsClick,
    onUpdate,
    onHideDetails,
    onEditClick,
    onEditCancelClick,
    editing
  }) => (
  <ReactCardFlip
    isFlipped={isFlipped}
    className="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2"
    style={{paddingLeft: 2, paddingRight: 2}}
  >
    <div className="col-12 px-3 py-4 border border-dark my-2"
         key="front"
         style={(amount === '0')
           ? {minHeight: 250, borderRadius: 5, boxShadow: ' 0px 4px 9px -4px #000', backgroundColor: '#eeeeee'}
           : {minHeight: 250, borderRadius: 5, boxShadow: ' 0px 4px 9px -4px #000'}
         }>
      <div className="center">
        <div className="col-12">
          <div className="imgContainer">
            {(image)
              ? <img
                src={image}
                alt={name}
                style={(amount === '0') ? {width: 75, height: 75, filter: 'blur(2px)' } : {width: 75, height: 75}}/>
              : <img
                  src="http://cumbrianrun.co.uk/wp-content/uploads/2014/02/default-placeholder.png"
                  alt="No photo"
                  style={(amount === '0') ? {width: 75, height: 75, filter: 'blur(2px)' } : {width: 75, height: 75}}
                />
            }
            {(amount === '0')
              ? <div style={{position:'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%) rotate(-30deg)',
                fontSize: 16,
                fontWeight: 700,
                }}
                >
                  Out of Stock
                </div>
              : null
            }
          </div>
        </div>
        <div className="col-12">
          <h4>{name}</h4>
        </div>
        <div className="col-12">
          <h6>Qty: {amount} {amountType}</h6>
        </div>
        <Button
          onClick={onDetailsClick}
          value={index}
          label="Show Details"
          className="btn btn-primary"
        />
      </div>
    </div>
    <div className="col-12 px-3 py-4 border border-dark" key="back"
         style={{minHeight: 250, borderRadius: 5, boxShadow: '0px 4px 9px -4px #000'}}>
      <div className="col-12 center">
        <h4 style={{borderBottom: '2px solid #e6e6e6'}}>{name}</h4>
      </div>
      {
        editing && (
          <EditForm
            amount={amount}
            amountType={amountType}
            id={id}
            onEditCancelClick={onEditCancelClick}
            formSubmit={onUpdate}
          />
        )
      }
      {
        !editing && Object.entries(nutrition).map((category, index) => (
            <p key={`${category[0]}${index}`}>{category[0]}: {category[1]}</p>
          )
        )
      }
      { !editing && (
        <div className="row m-0">
          <Button
            onClick={onHideDetails}
            className="btn btn-secondary col"
            title="Back"
            value={index}
            label={<span className="oi oi-arrow-thick-left" value={id}/>}
            style={{minWidth: 50}}
          />
          <Button
            onClick={onEditClick}
            className="btn btn-success margin-l-1 col"
            title="Edit"
            label={<span className="oi oi-pencil" value={id}/>}
            style={{minWidth: 50}}
          />
          <Button
            className="btn btn-danger margin-l-1 col"
            name={name}
            title="Delete"
            label={<span className="oi oi-trash" value={id} name={name}/>}
            onClick={onDelete}
            value={id}
            style={{minWidth: 50}}
          />
        </div>
      )}
    </div>
  </ReactCardFlip>
);

export default ItemDetails

ItemDetails.propTypes = {
  name: PropTypes.string,
  nutrition: PropTypes.object,
  amount: PropTypes.string,
  image: PropTypes.string,
  amountType: PropTypes.string,
  selected: PropTypes.string,
  index: PropTypes.string,
  onClick: PropTypes.func,
  id: PropTypes.string,
  onDelete: PropTypes.func,
  isFlipped: PropTypes.bool,
  onDetailsClick: PropTypes.func,
  onUpdate: PropTypes.func,
  onHideDetails: PropTypes.func,
  onEditClick: PropTypes.func,
  onEditCancelClick: PropTypes.func,
  editing: PropTypes.bool,
};

ItemDetails.defaultProps = {
  name: '',
  nutrition: {},
  amount: '',
  image: '',
  amountType: '',
  selected: '',
  index: '',
  onClick: noop,
  id: '',
  onDelete: noop,
  isFlipped: false,
  onDetailsClick: noop,
  onUpdate: noop,
  onHideDetails: noop,
  onEditClick: noop,
  onEditCancelClick: noop,
  editing: false,
};
