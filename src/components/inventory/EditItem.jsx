import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import isNumeric from 'validator/lib/isNumeric';
import Input from 'base-components/build/components/Input';
import Dropdown from 'base-components/build/components/Dropdown';
import Button from 'base-components/build/components/Button';
const isNegativeNum = num => num < 0;

export default class EditItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amount: parseInt(this.props.amount),
      amountType: this.props.amountType,
      id: this.props.id
    };
    this.onChange = this.onChange.bind(this);
    this.onFocus = this.onFocus.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.onAddClick = this.onAddClick.bind(this);
    this.onSubtractClick = this.onSubtractClick.bind(this);
    this.formSubmit = this.formSubmit.bind(this);
  }
  formSubmit() {
    this.props.formSubmit(this.state);
  }
  onFocus() {
    if(this.state.amount === '0') {
      this.setState({ amount: '' })
    }
  }
  onBlur() {
    if(this.state.amount.length === 0) {
      this.setState({ amount: '0' })
    }
    this.setState({ amount: parseInt(this.state.amount, 10)})
  }
  onChange(e) {
    const name = e.target.name;
    const value = e.target.value;
    let newState = {
      [name]: value
    };
    if(value.length === 0) {
      newState[name] = '0'
    }
    if(name === 'amountType' || isNumeric(newState[name])) {
      newState[name] = parseInt(newState[name], 10);
      this.setState(newState);
    }
  }
  onAddClick() {
    const newValue = parseInt(this.state.amount);
    this.setState({ amount: newValue + 1 })
  }
  onSubtractClick() {
    const newValue = this.state.amount - 1;
    if(!isNegativeNum(newValue)) {
      this.setState({ amount: newValue })
    }
  }
  render() {
    const { onEditCancelClick } = this.props;
    return(
      <div className="inventory-edit-form">
        <div className="row align-items-center m-0 ml-4">
          <span
            title="Minus"
            className="oi oi-minus btn p-2"
            style={{ border: '2px solid', borderRadius: 25, color: 'red' }}
            onClick={this.onSubtractClick}

          />
          <div className="col-7">
            <Input
              value={this.state.amount}
              name="amount"
              id="amount"
              type="text"
              pattern="[0-9]*"
              title="Please enter an amount"
              maxLength="5"
              placeholder="Amount"
              className="form-control"
              required
              onFocus={this.onFocus}
              onBlur={this.onBlur}
              onChange={this.onChange}
            />
          </div>
          <span
            title="Add"
            className="oi oi-plus btn p-2"
            style={{ border: '2px solid', borderRadius: 25, color: '#29a747'}}
            onClick={this.onAddClick}
          />
        </div>
        <div className="row m-0">
          <div className="col-12">
            <Dropdown
              name="amountType"
              value={this.state.amountType}
              onChange={this.onChange}
              className="form-control mt-2"
              options={['Quantity', 'oz', 'count']}
            />
          </div>
        </div>
        <div className="row mt-4 m-0">
          <Button
            title="Cancel"
            className={`btn btn-outline-dark mt-1 btnMax col`}
            onClick={onEditCancelClick}
            label="Cancel"
          />
          <Button
            title="Submit"
            className={`btn btn-primary mt-1 ml-1 styles.btnMax col`}
            onClick={this.formSubmit}
            label="Submit"
          />
        </div>
      </div>
    )
  }
}

EditItem.propTypes = {
  amount: PropTypes.string,
  amountType: PropTypes.string,
  id: PropTypes.string.isRequired,
  onEditCancelClick: PropTypes.func,
  formSubmit: PropTypes.func,
};

EditItem.defaultProps = {
  amount: '',
  amountType: '',
  onEditCancelClick: noop,
  formSubmit: noop,
};
