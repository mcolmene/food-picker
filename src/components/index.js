import Create from './inventory';
import NotFound from './404';

const Components = {
  Create,
  NotFound,
};

export default Components;
