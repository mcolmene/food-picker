import React from 'react';
import { Link } from 'react-router';

const About = () => (
    <div className="container">
        <h3 className="center">Grocery Inventory</h3>
        <h4>Description</h4>
        <p>
            This app will keep track of the food you have availble in your house. 
            You will then have the ability to enter your recipes and what ingredients are needed.
            Once you enter your recipes you can select one and the app will determine if you have enough
            to make it. If you don't, it will be able to tell you what you are missing and you will have 
            the opportunity to add it to your shopping cart. Once you have your shopping cart full of the
            things you need there will be an integration with local grocery stores. You will be able to get
            all your items at the click of a button and delivered to you whenever the store is availble to deliver.
        </p>
        <h4>Tech Stack</h4>
        <p>
           React, Mongo with mongoose, Node with Express, webpack.
        </p>
        <h4>MongoDB</h4>
        <p>
           Currently using mlab to host the mongo db.
        </p>
        <h4>Api</h4>
        <p>
            Using https://www.nutritionix.com/ api to: 
        </p>
        <ol style={{ wordBreak: 'break-word'}}>
            <li>To populate user search when creating their inventory (https://trackapi.nutritionix.com/v2/search/instant)</li>
            <li>To gather nutrient data about the food to display (https://www.nutritionix.com/track-api/v2/natural/nutrients)</li>            
        </ol>
        <h4>Helper Modules</h4>
        <ol>
            <li>
                <a href={"https://github.com/ericgio/react-bootstrap-typeahead"}>Typeahead</a>
            </li>
        </ol>
        <h4>Status: In Progress</h4>
        <p>
            Production of this app is under way and will continue to add features until I can complete an end to end flow
            of what was described above.
        </p>
        <Link to="/inventory">Check out the inventory list!</Link>
    </div>
)
export default About;