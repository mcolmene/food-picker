import React from 'react';
import PropTypes from 'prop-types';
import styles from '../static/foodpicker.css';

const Header = ({ title }) => (
    <div className="border-b center col-12">
        <h5 className={`${styles.header} pad-3`}>{title}</h5>
    </div>
);

export default Header;

Header.propTypes = {
  title: PropTypes.string,
};

Header.defaultProps = {
  title: 'title placeholder',
};
