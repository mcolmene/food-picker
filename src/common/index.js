import Container from './Container';
import Navbar from './Navbar';
import Header from './Header';
const common = {
  Container,
  Navbar,
  Header
};

export default common;
