import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Sidebar from 'react-sidebar';
import { Link } from 'react-router';
import Navbar from './Navbar';
import styles from '../static/foodpicker.css';

const mql = window.matchMedia('(min-width: 3000px)');

class Container extends Component {

  constructor(props) {
    super(props);

    this.state = {
      mql,
      docked: props.docked,
      open: props.open
    };

    this.mediaQueryChanged = this.mediaQueryChanged.bind(this);
    this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
  }

  componentWillMount() {
    mql.addListener(this.mediaQueryChanged);
    this.setState({ mql, sidebarDocked: mql.matches });
  }

  componentWillUnmount() {
    this.state.mql.removeListener(this.mediaQueryChanged);
  }
  onSetSidebarOpen(open) {
    this.setState({ sidebarOpen: open });
  }
  mediaQueryChanged() {
    this.setState({ sidebarDocked: this.state.mql.matches });
  }

  render() {
    var sidebarContent = (
      <div>
          <div className="bg-primary p-2">
            <h3>Menu</h3>
          </div>
          <Link to="/">
            <div className="col-12 py-3">
              About
            </div>
          </Link>
          <Link to="/inventory">
            <div className="col-12 py-3">
              Inventory
            </div>
          </Link>
          <Link to="/recipe">
            <div className="col-12 py-3">
              Recipes
            </div>
          </Link>
      </div>
      );

    return (
        <Sidebar
          sidebarClassName={styles.sidebar}
          sidebar={sidebarContent}
          open={this.state.sidebarOpen}
          docked={this.state.sidebarDocked}
          onSetOpen={this.onSetSidebarOpen}
        >
          <Navbar onSetSidebarOpen={this.onSetSidebarOpen} />
          <div className="wrapper">
            { this.props.children }
          </div>
        </Sidebar>
      );
  }
  }

export default Container;

Container.propTypes = {
  docked: PropTypes.bool,
  open: PropTypes.bool,
  children: PropTypes.node,

};
