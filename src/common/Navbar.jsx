import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import { noop } from 'lodash';
import Button from 'base-components/build/components/Button';

const Navbar = (props) => (
  <div>
    <div className="d-none d-sm-block p-4">
      <span className="col-sm-1 pad-4">
        <Link to="/" className="">About</Link>
      </span>
      <span className="col-sm-1 pad-4">
        <Link to="/inventory" className="">Inventory</Link>
      </span>
      <span className="col-sm-1 pad-4">
        <Link to="/recipe" className="">Recipes</Link>
      </span>
    </div>
    <div className="row margin-0 d-sm-none">
      <div className="col-12 py-3">
        <Button
          onClick={props.onSetSidebarOpen}
          label={<span className="oi oi-menu" />}
          className="d-sm-none btn btn-primary float-l"
        />
      </div>
    </div>
  </div>
);
export default Navbar;

Navbar.propTypes = {
  onSetSidebarOpen: PropTypes.func,
};

Navbar.defaultProps = {
  onSetSidebarOpen: noop,
};
