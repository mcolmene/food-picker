import React from 'react';
import PropTypes from 'prop-types';
import Popover from 'react-popover/build';

const PopoverElement = ({ isOpen, content, children, placement }) => {
  const popoverProps = {
    isOpen,
    preferPlace: placement,
    place: placement,
    body: [content]
  };
  return (
    <Popover {...popoverProps}>
      {children}
    </Popover>
  );
};

export default PopoverElement;

PopoverElement.propTypes = {
  isOpen: PropTypes.bool,
  content: PropTypes.node,
  children: PropTypes.node,
  placement: PropTypes.string,
};

PopoverElement.defaultProps = {
  isOpen: false,
  content: null,
  children: null,
  placement: 'below',
};
