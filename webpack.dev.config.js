var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: 'source-map',
  entry: './index.dev.js',
  output: {
    path: path.join(__dirname, '/public'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        include: [
          path.join(__dirname, 'index.js'),
          path.join(__dirname, 'index.dev.js'),
          path.join(__dirname, 'server'),
          path.join(__dirname, 'src'),
          path.join(__dirname, 'test'),
        ]
      },
      { test: /\.s?css$/,
        loader:
        'style-loader!' +
        'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!' +
        'sass-loader!import-glob-loader'
      },
      {
        test: /\.woff|\.woff2|\.svg|.eot|\.ttf/,
        loader: 'url?prefix=font/&limit=10000'
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      { test: /\.(jpe?g|png|gif|svg)$/i,
        loader: 'file-loader'
      }
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx', '.scss']
  }
};
